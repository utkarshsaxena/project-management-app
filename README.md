# README #

## Introduction:  

This is the fourth summer projects I will be developing using Ruby on Rails. Primary aim of these projects is to focus on learning Ruby on Rails and apply my knowledge to implement complex features in my web application.

## Technologies: ##

* HTML/ERB Templating
* CSS/Bootstrap
* Ruby on Rails
* Stripe
* SendGrid
* AWS S3 Bucket
* Milia Gem - Multi-tenant


## Purpose: ##

* Become proficient in Ruby on Rails.
* Be well equipped for my Fall '16 internship with Shopify.

## Procedure: ##

1. Install [Ruby on Rails.](http://rubyonrails.org/)
2. Browse to the root folder and run the following command in the terminal: rails server -p 0.0.0.0 or rails server


## Timestamp: ##

**July, 2016